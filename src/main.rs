#![cfg_attr(feature = "pedantic", warn(clippy::pedantic))]
#![warn(clippy::use_self)]
#![warn(deprecated_in_future)]
#![warn(future_incompatible)]
#![warn(missing_debug_implementations)]
#![warn(rust_2018_compatibility)]
#![warn(rust_2018_idioms)]
#![warn(unreachable_pub)]
#![warn(unused)]
#![deny(warnings)]

use std::convert::Infallible;
use std::net::Ipv4Addr;

use futures::future;
use warp::Filter as _;

use crate::server::TIN;

use crate::server::{Add, Subtract};

mod server;

// async fn main() {
//     let srv = JsonRpcServer;
//     println!("Hello, world! I am {:?}", srv);
// }

#[tokio::main]
async fn main() {
    env_logger::init();
    // create Filter
    // let route = warp::filters::path::path("rpc")
    //     // ## Point 1
    //     // This filter is required.
    //     .and(json_rpc::json_rpc())
    //     .and(json_rpc::method(Add::METHOD))
    //     .and(json_rpc::params::<Add>())
    //     // `res.success` returns `impl Reply` which represents JSON RPC Response
    //     .map(|builder: Builder, request: Add| Arc::clone(&*TIN).call(builder, request).unwrap());

    let route = warp::filters::path::path("rpc")
        .and(TIN.add_success::<Add>().or(TIN.add_success::<Subtract>()))
        .with(warp::log("JSONRPC"));

    // ## Point 2
    // You **MUST** wraps root `Filter` by `warp_json_rpc::service` function.
    let svc = warp_json_rpc::service(route);
    let make_svc =
        hyper::service::make_service_fn(move |_| future::ok::<_, Infallible>(svc.clone()));
    hyper::Server::bind(&(Ipv4Addr::LOCALHOST, 3030).into())
        // hyper::Server::bind("localhost:3030".into())
        .serve(make_svc)
        .await
        .unwrap();
}
