use std::sync::Arc;

use hyper::{http, Body};
use lazy_static::lazy_static;
use serde::{de, Deserialize, Serialize};
use warp::{Filter, Rejection};
use warp_json_rpc::{filters as json_rpc, Builder};

lazy_static! {
    pub(crate) static ref TIN: Arc<Tin> = Arc::new(Tin::new());
}

pub(crate) trait JsonRpc: de::DeserializeOwned + Send {
    const METHOD: &'static str;
    type Response: 'static + Serialize + Sized;
    fn call(self) -> Self::Response;
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub(crate) struct Tin {
    calls: u64,
}

impl Tin {
    pub(crate) fn new() -> Self {
        Self::default()
    }

    pub(crate) fn add_success<M>(
        &self,
    ) -> impl Filter<Extract = (http::Response<Body>,), Error = Rejection> + Copy + Sized
    where
        M: JsonRpc + Sized,
        // F: Filter + Copy,
        // F::Error: Reject,
    {
        json_rpc::json_rpc()
            .and(json_rpc::method(M::METHOD))
            .and(json_rpc::params::<M>())
            .map(|builder: Builder, request: M| {
                let content = request.call();
                builder.success(content).unwrap()
            })
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Add {
    lhs: u64,
    rhs: u64,
}

impl JsonRpc for Add {
    const METHOD: &'static str = "add";
    type Response = u64;
    fn call(self) -> Self::Response {
        self.lhs + self.rhs
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Subtract {
    lhs: u64,
    rhs: u64,
}

impl JsonRpc for Subtract {
    const METHOD: &'static str = "subtract";
    type Response = u64;
    fn call(self) -> Self::Response {
        self.lhs - self.rhs
    }
}
